// environment specific settings
environments {
    development {
        dataSource {
            hibernate {
                cache.use_second_level_cache = true
                cache.use_query_cache = false
                cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
                format_sql = true
                use_sql_comments = true
            }

            logSql = true
            pooled = true
            dialect = org.hibernate.dialect.PostgreSQLDialect
            driverClassName = "org.postgresql.Driver"
            dbCreate = "update"
            url = "jdbc:postgresql://127.0.0.1:5432/elasticsearch_plugin"
            username = "postgres"
            password = "postgres"
        }
    }
    test {
        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = true
            cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
        }

        dataSource {
            pooled = true
            driverClassName = "org.h2.Driver"
            username = "sa"
            password = ""

            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE"
        }
    }
}
