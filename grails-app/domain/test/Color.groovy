package test

class Color {

    String name
    Integer red
    Integer green
    Integer blue

    static constraints = {
        name blank: false
    }

    static elasticSearchable = {
        root false
        only = [ 'name' ]
    }

}
