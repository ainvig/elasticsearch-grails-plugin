package test

class GeoPoint {

    Double lat
    Double lon

    static elasticSearchable = {
        root false
    }
}
